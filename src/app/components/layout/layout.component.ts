import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  public options = [
    { title: 'Buttons', redirect: 'buttons' },
    { title: 'Todo List', redirect: 'todo' },
    { title: 'Indicadores', redirect: 'indicator' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
