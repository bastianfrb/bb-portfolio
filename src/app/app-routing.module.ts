import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/app',
    pathMatch: 'full',
  },
  {
    path: 'app',
    component: AppComponent,
  },
  {
    path: 'buttons',
    loadChildren: () => import('./modules/buttons/buttons.module').then(m => m.ButtonsModule),
  },
  {
    path: 'todo',
    loadChildren: () => import('./modules/todo-list/todo-list.module').then(m => m.TodoListModule),
  },
  {
    path: 'indicator',
    loadChildren: () => import('./modules/indicator/indicator.module').then(m => m.IndicatorModule),
  },
  {
    path: '**',
    redirectTo: '/app'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
