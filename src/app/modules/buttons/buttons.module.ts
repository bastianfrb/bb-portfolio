import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { ButtonsRoutingModule } from './buttons-routing.module';
import { ButtonContainerComponent } from './components/button-container/button-container.component';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    ButtonComponent,
    ButtonContainerComponent
  ],
  imports: [
    CommonModule,
    ButtonsRoutingModule,
    MatRippleModule,
    MatIconModule
  ],
  exports: [
  ]
})
export class ButtonsModule { }
