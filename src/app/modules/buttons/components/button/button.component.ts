import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bbutton',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() public variant = '';
  @Input() public disableShadow: any = null;
  @Input() public disabled: any = null;
  @Input() public startIcon = '';
  @Input() public endIcon = '';
  @Input() public size = 'md';
  @Input() public color = '';

  constructor() { }

  ngOnInit(): void {
  }

}
