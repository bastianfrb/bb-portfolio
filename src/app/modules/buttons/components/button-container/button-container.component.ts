import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-container',
  templateUrl: './button-container.component.html',
  styleUrls: ['./button-container.component.scss']
})
export class ButtonContainerComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit(): void {
    // this.setHoverState();
  }

  ngAfterViewInit(): void {
    this.setHoverState();
  }

  private setHoverState(): void {

  }



}
