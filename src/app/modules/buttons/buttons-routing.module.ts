import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ButtonContainerComponent } from "./components/button-container/button-container.component";

const routes: Routes = [
    {
      path: '',
      component: ButtonContainerComponent,
    },
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class ButtonsRoutingModule {}
  