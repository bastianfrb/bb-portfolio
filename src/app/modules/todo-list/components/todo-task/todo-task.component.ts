import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CacheService } from 'src/app/modules/shared/services/cache.service';
import { ITodoTask } from '../../interfaces/todo-task';

@Component({
  selector: 'app-todo-task',
  templateUrl: './todo-task.component.html',
  styleUrls: ['./todo-task.component.scss']
})
export class TodoTaskComponent {

  @Input() public task: ITodoTask;

  @Output() public onChangeTasks: EventEmitter<ITodoTask[]>;

  constructor(private cacheService: CacheService) {
    this.onChangeTasks = new EventEmitter<ITodoTask[]>();
  }

  /**
   * Function to change current task status
   */
  public changeTaskStatus(): void {
    const tasks = this.cacheService.local('tasks') as ITodoTask[];
    const newTasks = tasks.map((v) => {
      if (v.title === this.task.title) {
        v.isDone = !this.task.isDone;
      }

      return v;
    });

    this.changeTasks(newTasks);
  }

  /**
   * Function to delete task from todo list
   */
  public deleteTask(): void {
    const tasks = this.cacheService.local('tasks') as ITodoTask[];
    const newTasks = tasks.filter((v) => v.title !== this.task.title);
    this.changeTasks(newTasks);
  }

  /**
   * Function to save tasks on local storage and emit event
   * @param tasks Tasks to save
   */
  public changeTasks(tasks: ITodoTask[]): void {
    this.cacheService.saveLocal('tasks', tasks);
    this.onChangeTasks.emit(tasks);
  }
}
