import { Component, OnInit } from '@angular/core';
import { CacheService } from 'src/app/modules/shared/services/cache.service';
import { ITodoTask } from '../../interfaces/todo-task';

@Component({
  selector: 'app-main-todo',
  templateUrl: './main-todo.component.html',
  styleUrls: ['./main-todo.component.scss']
})
export class MainTodoComponent implements OnInit {

  public tasks: ITodoTask[];

  public taskTitle: string;

  constructor(private cacheService: CacheService) {
    this.tasks = [];
  }

  ngOnInit(): void {
    this.tasks = this.cacheService.local('tasks');
  }

  public addTask(): void {
    const title = this.taskTitle.trim();

    if (title) {
      if (!this.tasks) {
        this.tasks = [];
      }
  
      this.tasks.push({ title, isDone: false });
      this.cacheService.saveLocal('tasks', this.tasks);

      this.taskTitle = '';
    }
  }

}
