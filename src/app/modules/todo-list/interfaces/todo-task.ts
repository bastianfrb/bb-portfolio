export interface ITodoTask {
    title: string;
    isDone: boolean;
}