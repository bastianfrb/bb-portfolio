import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainTodoComponent } from './components/main-todo/main-todo.component';
import { TodoListRoutingModule } from './todo-list-routing.module';
import { MatCardModule } from '@angular/material/card';
import { TodoTaskComponent } from './components/todo-task/todo-task.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';


@NgModule({
  declarations: [
    MainTodoComponent,
    TodoTaskComponent
  ],
  imports: [
    CommonModule,
    TodoListRoutingModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    MatDividerModule
  ]
})
export class TodoListModule { }
