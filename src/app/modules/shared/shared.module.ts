import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ApiCallService } from './services/api-call.service';
import { DatePresentationPipe } from './pipes/date-presentation.pipe';

@NgModule({
  declarations: [DatePresentationPipe],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [DatePresentationPipe],
  providers: [ApiCallService]
})
export class SharedModule { }
