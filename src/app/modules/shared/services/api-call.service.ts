import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private http: HttpClient) {}

  public get(url: string, params?: any): Observable<any> {
    return this.http.get(url, params);
  }

  public post(url: string, body?: any, params?: any): Observable<any> {
    return this.http.post(url, body || null, params);
  }
}
