import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  constructor() { }

  public local(key: string): any {
    try {
      return JSON.parse(localStorage.getItem(key) || '');
    } catch (error) {
      return null;
    }
  }

  public saveLocal(key: string, value: any): void {
    try {
      const asd = JSON.stringify(value);
      localStorage.setItem(key, asd);
    } catch (error) {
      console.log(error);
    }
  }

  public clearLocal(key?: string): void {
    if (key) {
      localStorage.removeItem(key);
    } else {
      localStorage.clear();
    }
  }

  public session(key: string): any {
    try {
      return JSON.parse(sessionStorage.getItem(key) || '');
    } catch (error) {
      return null;
    }
  }

  public saveSession(key: string, value: any): any {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  public clearSession(key?: string): void {
    if (key) {
      sessionStorage.removeItem(key);
    } else {
      sessionStorage.clear();
    }
  }


  public getAllSession(): any {
    return Object.keys(sessionStorage).reduce((obj: any, key: string) => {
      try {
        obj[key] = JSON.parse(sessionStorage.getItem(key) || '');
        return obj;
      } catch (error) {
        return null;
      }
   }, {});
  }

  public getAllLocal() {
    return Object.keys(localStorage).reduce((obj: any, key: string) => {
      obj[key] = localStorage.getItem(key);
      return obj;
   }, {});
  }
}
